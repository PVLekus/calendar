package ru.test.calendar.di.base;

public interface PageComponentBuilder<C extends PageComponent, M extends PageModule>   {
    C build();
    PageComponentBuilder<C,M> makeModule(M module);
}
