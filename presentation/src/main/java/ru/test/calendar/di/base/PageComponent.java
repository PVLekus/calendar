package ru.test.calendar.di.base;

public interface PageComponent<A> {
    void inject(A page);
}
