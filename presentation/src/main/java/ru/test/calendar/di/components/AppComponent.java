package ru.test.calendar.di.components;

import dagger.Component;
import ru.test.calendar.di.base.ComponentsHolder;
import ru.test.calendar.di.base.PerApp;
import ru.test.calendar.di.modules.AppModule;


@PerApp
@Component(modules = {AppModule.class})
public interface AppComponent {
    ComponentsHolder injectComponentsHolder(ComponentsHolder componentsHolder);
}
