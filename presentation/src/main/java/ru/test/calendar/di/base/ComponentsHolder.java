package ru.test.calendar.di.base;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import ru.test.calendar.CalendarApp;
import ru.test.calendar.di.components.AppComponent;
import ru.test.calendar.di.components.DaggerAppComponent;
import ru.test.calendar.di.modules.AppModule;

/**
 * Created by oleg on 13.01.18.
 */

public class ComponentsHolder {

    private final CalendarApp app;

    @Inject
    Map<Class<?>, Provider<PageComponentBuilder>> builders;

    private Map<Class<?>, PageComponent> components;
    private AppComponent appComponent;

    public ComponentsHolder(CalendarApp app) {
        this.app = app;
    }

    public void initHolder() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(app)).build();
        appComponent.injectComponentsHolder(this);
        components = new HashMap<>();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


    public PageComponent getPageComponent(Class<?> cls) {
        return getPageComponent(cls, null);
    }

    public PageComponent getPageComponent(Class<?> cls, PageModule module) {
        PageComponent component = components.get(cls);
        if (component == null) {
            PageComponentBuilder builder = builders.get(cls).get();
            if (module != null) {
                builder.makeModule(module);
            }
            component = builder.build();
            components.put(cls, component);
        }
        return component;
    }

    public void releasePageComponent(Class<?> cls) {
        components.put(cls, null);

    }

}
