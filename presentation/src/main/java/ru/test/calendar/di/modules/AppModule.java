package ru.test.calendar.di.modules;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import ru.test.calendar.di.base.PageComponentBuilder;
import ru.test.calendar.di.base.PerApp;
import ru.test.calendar.CalendarApp;
import ru.test.calendar.pages.month.MonthFragment;
import ru.test.calendar.pages.month.di.MonthFragmentComponent;
import ru.test.calendar.pages.main.MainActivity;
import ru.test.calendar.pages.main.di.MainActivityComponent;
import ru.test.data.entity.ProductionСalendarEntity;
import ru.test.data.repo.MonthRepository;
import ru.test.data.utils.JsonLoader;
import ru.test.domain.repo.IMonthsRepository;
import ru.test.domain.utils.MonthUtil;

/**
 * Created by oleg on 13.01.18.
 */

@Module(subcomponents = {MainActivityComponent.class, MonthFragmentComponent.class})
public class AppModule {

    private final CalendarApp app;

    public AppModule(CalendarApp app) {
        this.app = app;
    }

    @Provides
    @PerApp
    Application provideApplication() {
        return app;
    }

    @Provides
    InputMethodManager provideInputMethodManager() {
        return  (InputMethodManager) app.getSystemService(Activity.INPUT_METHOD_SERVICE);
    }

    @PerApp
    @Provides
    IMonthsRepository providesMonthsRepository(ProductionСalendarEntity productionСalendar, MonthUtil monthUtil) {
        return new MonthRepository(productionСalendar, monthUtil);
    }

    @PerApp
    @Provides
    MonthUtil provideMonthUtil() {
        return new MonthUtil();
    }

    @PerApp
    @Provides
    ProductionСalendarEntity provideProductionСalendar(Context context) {

        JsonLoader loader = new JsonLoader(context);
        String json = loader.loadJson("years_json.json");

        if (json != null) {
            return new Gson().fromJson(json, ProductionСalendarEntity.class);
        } else {
            return new ProductionСalendarEntity();
        }

    }

    @PerApp
    @Provides
    Context provideContext() { return app.getApplicationContext(); }

    @Provides
    @IntoMap
    @ClassKey(MainActivity.class)
    PageComponentBuilder provideMainActivity(MainActivityComponent.Builder builder) {
        return builder;
    }

    @Provides
    @IntoMap
    @ClassKey(MonthFragment.class)
    PageComponentBuilder provideMonthFragment(MonthFragmentComponent.Builder builder) {
        return builder;
    }

}
