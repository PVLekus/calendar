package ru.test.calendar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 13.01.18.
 */

public abstract class SimpleAdapter<T> extends RecyclerView.Adapter<BaseVH<T>> {

    int layout;
    List<T> list = new ArrayList<T>();

    public SimpleAdapter(int layout) {
        this.layout = layout;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(BaseVH<T> holder, int position) {
        holder.fill(list.get(position));
    }

    public void update(List<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public BaseVH<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        return getHolder(LayoutInflater.from(parent.getContext()).inflate(layout,parent, false));
    }

    public abstract BaseVH<T> getHolder(View view);

}
