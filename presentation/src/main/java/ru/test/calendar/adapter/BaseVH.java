package ru.test.calendar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by oleg on 13.01.18.
 */

public abstract class BaseVH<T> extends RecyclerView.ViewHolder {


    public BaseVH(View itemView) {
        super(itemView);
    }

    public abstract void fill(T item);

}
