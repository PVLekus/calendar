package ru.test.calendar.adapter;

import android.view.View;

import java.util.List;

/**
 * Created by oleg on 13.01.18.
 */

public interface IAdapter<T> {

    void updateAdapter(List<T> list);

}
