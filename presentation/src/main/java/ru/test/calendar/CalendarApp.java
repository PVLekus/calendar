package ru.test.calendar;

import android.app.Application;

import java.util.Calendar;

import ru.test.calendar.di.base.ComponentsHolder;

/**
 * Created by oleg on 13.01.18.
 */

public class CalendarApp extends Application {

    public static CalendarApp INSTANCE;
    private ComponentsHolder componentsHolder;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        initComponentsHolder();
    }

    private void initComponentsHolder() {
        componentsHolder = new ComponentsHolder(this);
        componentsHolder.initHolder();
    }

    public ComponentsHolder getComponentsHolder() {
        return componentsHolder;
    }

}
