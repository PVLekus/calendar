package ru.test.calendar.pages.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.test.calendar.mvp.BasePresenter;
import ru.test.domain.utils.MonthUtil;
import rx.Observable;

/**
 * Created by oleg on 13.01.18.
 */

public class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    SimpleDateFormat format;
    MonthUtil util;

    public MainPresenter(SimpleDateFormat format, MonthUtil util) {
        this.format = format;
        this.util = util;
    }

    @Override
    public void viewIsReady() {

    }

    @Override
    public void updateViewPagerItem() {

        compositeSubscription.add(Observable.just(new Date())
                .map(util::getMonthNumber)
                .subscribe(integer -> {
                    if (view != null) view.selectViewPagerItem(integer-1);
                }, throwable -> {
                    if (throwable != null) throwable.printStackTrace();
                    if (view != null) view.showToast("Ошибка получения месяца");
                }));

    }

    @Override
    public void setTitle(int monthNumber) {

        Calendar calendar = new GregorianCalendar();
        calendar.set(2015, Calendar.DECEMBER, 1);
        calendar.add(Calendar.MONTH, monthNumber);

        String month = format.format(calendar.getTime());
        String title = month.substring(0, 1).toUpperCase() + month.substring(1);

        if (view != null) view.setTitle(title);
    }
}
