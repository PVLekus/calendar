package ru.test.calendar.pages.main.di;

import java.text.SimpleDateFormat;
import java.util.Locale;

import dagger.Module;
import dagger.Provides;
import ru.test.calendar.di.base.PageModule;
import ru.test.calendar.di.base.PerActivity;
import ru.test.calendar.pages.main.MainContract;
import ru.test.calendar.pages.main.MainPresenter;
import ru.test.domain.utils.MonthUtil;

/**
 * Created by oleg on 13.01.18.
 */
@Module
public class MainActivityModule implements PageModule {

    @PerActivity
    @Provides
    MainContract.Presenter providePresenter (MonthUtil util) {

        return new MainPresenter(new SimpleDateFormat("LLLL ''yy", Locale.getDefault()), util);
    }

}
