package ru.test.calendar.pages.main.di;

import dagger.Subcomponent;
import ru.test.calendar.di.base.PageComponent;
import ru.test.calendar.di.base.PageComponentBuilder;
import ru.test.calendar.di.base.PerActivity;
import ru.test.calendar.pages.main.MainActivity;

/**
 * Created by oleg on 13.01.18.
 */
@PerActivity
@Subcomponent(modules = {MainActivityModule.class})
public interface MainActivityComponent extends PageComponent<MainActivity> {

    @Subcomponent.Builder
    interface Builder extends PageComponentBuilder<MainActivityComponent, MainActivityModule>{

    }

}
