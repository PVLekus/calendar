package ru.test.calendar.pages.month.di;

import dagger.Subcomponent;
import ru.test.calendar.di.base.PageComponent;
import ru.test.calendar.di.base.PageComponentBuilder;
import ru.test.calendar.di.base.PerFragment;
import ru.test.calendar.pages.month.MonthFragment;

/**
 * Created by oleg on 13.01.18.
 */
@PerFragment
@Subcomponent(modules = {MonthFragmentModule.class})
public interface MonthFragmentComponent extends PageComponent<MonthFragment> {

    @Subcomponent.Builder
    interface Builder extends PageComponentBuilder<MonthFragmentComponent, MonthFragmentModule> {

    }

}
