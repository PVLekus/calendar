package ru.test.calendar.pages.month.di;

import dagger.Module;
import dagger.Provides;
import ru.test.calendar.di.base.PageModule;
import ru.test.calendar.di.base.PerFragment;
import ru.test.calendar.pages.month.MonthContract;
import ru.test.calendar.pages.month.MonthPresenter;
import ru.test.domain.repo.IMonthsRepository;

/**
 * Created by oleg on 13.01.18.
 */
@Module
public class MonthFragmentModule implements PageModule {

    @PerFragment
    @Provides
    MonthContract.Presenter providePresenter(IMonthsRepository monthsRepository) {
        return new  MonthPresenter(monthsRepository);
    }

}
