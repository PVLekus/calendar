package ru.test.calendar.pages.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.test.calendar.pages.month.MonthFragment;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthPagerAdapter extends FragmentStatePagerAdapter {

    private static final int MONTH_COUNT = 36;

    public MonthPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return MonthFragment.instance(position+1);
    }

    @Override
    public int getCount() {
        return MONTH_COUNT;
    }
}
