package ru.test.calendar.pages.main;

import ru.test.calendar.mvp.MvpPresenter;
import ru.test.calendar.mvp.MvpView;

/**
 * Created by oleg on 13.01.18.
 */

public class MainContract {

    public interface View extends MvpView {
        void setTitle(String title);
        void selectViewPagerItem(int item);
    }

    public interface Presenter extends MvpPresenter<View> {
        void updateViewPagerItem();
        void setTitle(int monthNumber);
    }

}
