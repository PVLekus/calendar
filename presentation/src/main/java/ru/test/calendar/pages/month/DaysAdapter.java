package ru.test.calendar.pages.month;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.test.calendar.R;
import ru.test.calendar.adapter.BaseVH;
import ru.test.calendar.adapter.SimpleAdapter;
import ru.test.domain.data.entity.Day;

/**
 * Created by oleg on 13.01.18.
 */

public class DaysAdapter extends SimpleAdapter<Day> {

    public DaysAdapter() {
        super(R.layout.item_day);
    }

    @Override
    public BaseVH<Day> getHolder(View view) {
        return new DaysVH(view);
    }

     static class DaysVH extends BaseVH<Day> {

        @BindView(R.id.day_number) TextView dayNumber;
        @BindView(R.id.mask) ImageView mask;

        DaysVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void fill(Day item) {

            dayNumber.setText(String.valueOf(item.getNumber()));

            if (!item.isInMonth()
                    && ( item.getState() == Day.DayType.WEEKEND || item.getState() == Day.DayType.HOLIDAY || item.getState() == Day.DayType.SHORT_DAY)) {
                mask.setVisibility(View.VISIBLE);
            } else {
                mask.setVisibility(View.INVISIBLE);
            }

            dayNumber.setTextColor(ContextCompat.getColor(itemView.getContext(), getTextColor(item)));

            switch (item.getState()) {
                case HOLIDAY:
                    dayNumber.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.shape_holiday));
                    break;
                case WEEKEND:
                    dayNumber.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.shape_weekend));
                    break;
                case SHORT_DAY:
                    dayNumber.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.shape_short));
                    break;
                case WORK_DAY:
                    dayNumber.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.shape_work));
                    break;
            }

        }

        private int getTextColor(Day item) {

            if (item.getState() == Day.DayType.HOLIDAY || item.getState() == Day.DayType.WEEKEND) {
                return R.color.holiday_text_color;
            } else if (item.getState() == Day.DayType.SHORT_DAY) {
                return R.color.short_day_text_color;
            } else if (!item.isInMonth()) {
                return R.color.light_gray_text_color;
            } else {
                return R.color.dark_text_color;
            }

        }

    }

}
