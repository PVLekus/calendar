package ru.test.calendar.pages.month;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.test.calendar.CalendarApp;
import ru.test.calendar.R;
import ru.test.calendar.mvp.BaseFragment;
import ru.test.domain.data.entity.Day;
import ru.test.domain.data.entity.Month;
import ru.test.domain.data.entity.WeekType;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthFragment extends BaseFragment implements MonthContract.View {

    private static final String MONTH_NUMBER = "month_number";
    private int monthNumber;
    private DaysAdapter adapter;
    private LayoutInflater layoutInflater;
    @Inject MonthContract.Presenter presenter;

    @BindView(R.id.daysList) RecyclerView daysList;
    @BindView(R.id.main_info) TextView mainInfo;
    @BindView(R.id.holidays_container) LinearLayout holidaysContainer;
    @BindView(R.id.days_count_container) LinearLayout daysCountContainer;
    @BindView(R.id.week_hours_container) LinearLayout weekHoursContainer;

    public static MonthFragment instance(int month_number) {

        MonthFragment fragment = new MonthFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(MONTH_NUMBER, month_number);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        monthNumber = getArguments().getInt(MONTH_NUMBER);
        CalendarApp.INSTANCE.getComponentsHolder().getPageComponent(this.getClass()).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month, container, false);
        layoutInflater = inflater;
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new DaysAdapter();
        presenter.attachView(this);

        daysList.setLayoutManager(new GridLayoutManager(getContext(), 7));
        daysList.setAdapter(adapter);

        presenter.loadMonthInfo(monthNumber);
    }

    @Override
    public void updateAdapter(List<Day> list) {
        adapter.update(list);
    }

    @Override
    public void updateView(Month month) {

        mainInfo.setText(getString(R.string.main_info, month.getYear(), month.getHalfYear(), month.getQuarter()));

        for (String s: month.getHolidaysNames()) {
            holidaysContainer.addView(getStatView(s, null));
        }

        if (holidaysContainer.getChildCount() == 0) {
            holidaysContainer.addView(getStatView(getString(R.string.no_hildays), null));
        }

        daysCountContainer.addView(getStatView(getString(R.string.calendar_days), String.valueOf(month.getCalendarDays())));
        daysCountContainer.addView(getStatView(getString(R.string.work_days), String.valueOf(month.getWorkDays())));
        daysCountContainer.addView(getStatView(getString(R.string.weekend_and_holidays_days), String.valueOf(month.getHolidayAndWeekendDays())));

        weekHoursContainer.addView(getStatView(getString(R.string.week_40), String.valueOf(month.getWorkHours(WeekType.HOURS40))));
        weekHoursContainer.addView(getStatView(getString(R.string.week_36), String.valueOf(month.getWorkHours(WeekType.HOURS36))));
        weekHoursContainer.addView(getStatView(getString(R.string.week_24), String.valueOf(month.getWorkHours(WeekType.HOURS24))));
    }

    private View getStatView(String title, String data) {

        View vieStat = layoutInflater.inflate(R.layout.item_stat, null);
        if (title != null) ((TextView)vieStat.findViewById(R.id.title)).setText(title);
        if (data != null) ((TextView)vieStat.findViewById(R.id.data)).setText(data);
        else  vieStat.findViewById(R.id.data).setVisibility(View.GONE);

        return vieStat;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        CalendarApp.INSTANCE.getComponentsHolder().releasePageComponent(this.getClass());
    }
}
