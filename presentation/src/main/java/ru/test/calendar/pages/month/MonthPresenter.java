package ru.test.calendar.pages.month;

import ru.test.calendar.mvp.BasePresenter;
import ru.test.domain.repo.IMonthsRepository;
import rx.Observable;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthPresenter extends BasePresenter<MonthContract.View> implements MonthContract.Presenter {

    IMonthsRepository monthsRepository;

    public MonthPresenter(IMonthsRepository monthsRepository) {
        this.monthsRepository = monthsRepository;
    }

    @Override
    public void viewIsReady() {

    }

    @Override
    public void loadMonthInfo(int monthNumber) {

        compositeSubscription.add(Observable.just(monthNumber)
                .map(monthsRepository::getMonthByNumber)
                .subscribe(month -> {
                    if (view != null) {
                        view.updateView(month);
                        view.updateAdapter(month.getDays());
                    }
                }, throwable -> {
                    if (throwable != null) throwable.printStackTrace();
                    if (view != null) view.showToast("Ошибка загрузки страницы");
                }));


    }
}
