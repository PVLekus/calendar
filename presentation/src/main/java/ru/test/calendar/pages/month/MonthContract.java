package ru.test.calendar.pages.month;

import ru.test.calendar.adapter.IAdapter;
import ru.test.calendar.mvp.MvpPresenter;
import ru.test.calendar.mvp.MvpView;
import ru.test.domain.data.entity.Day;
import ru.test.domain.data.entity.Month;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthContract {

    public interface View extends MvpView, IAdapter<Day> {
        void updateView(Month month);
    }

    public interface Presenter extends MvpPresenter<View> {
        void loadMonthInfo(int monthNumber);
    }

}
