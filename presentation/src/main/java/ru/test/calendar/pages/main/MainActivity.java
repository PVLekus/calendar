package ru.test.calendar.pages.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.test.calendar.CalendarApp;
import ru.test.calendar.R;


/**
 * Created by oleg on 13.01.18.
 */

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.title) TextView toolbarTitle;

    @Inject MainContract.Presenter presenter;

    private MonthPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CalendarApp.INSTANCE.getComponentsHolder().getPageComponent(this.getClass()).inject(this);
        ButterKnife.bind(this);

        presenter.attachView(this);

        adapter = new MonthPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                presenter.setTitle(position+1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        presenter.updateViewPagerItem();
    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    @Override
    public void selectViewPagerItem(int item) {
        viewPager.setCurrentItem(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
        CalendarApp.INSTANCE.getComponentsHolder().releasePageComponent(this.getClass());
    }
}
