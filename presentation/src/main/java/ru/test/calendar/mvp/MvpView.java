package ru.test.calendar.mvp;

/**
 * Created by oleg on 13.01.18.
 */

public interface MvpView {
    void showToast(String message);
}
