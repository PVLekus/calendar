package ru.test.calendar.mvp;

import android.support.v4.app.Fragment;
import android.widget.Toast;

/**
 * Created by oleg on 13.01.18.
 */

public abstract class BaseFragment extends Fragment implements MvpView {

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
