package ru.test.calendar.mvp;

/**
 * Created by oleg on 13.01.18.
 */

public interface MvpPresenter<V extends MvpView> {

    void attachView(V view);
    void viewIsReady();
    void detachView();
    void destroy();

}
