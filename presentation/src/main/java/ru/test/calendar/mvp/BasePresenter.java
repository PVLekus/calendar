package ru.test.calendar.mvp;

import android.support.annotation.Nullable;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by oleg on 13.01.18.
 */

public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    @Nullable
    public V view;
    public CompositeSubscription compositeSubscription = new CompositeSubscription();

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void destroy() {
        detachView();
        compositeSubscription.clear();
        compositeSubscription = new  CompositeSubscription();
    }
}