package ru.test.calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;

import ru.test.calendar.pages.main.MainContract;
import ru.test.calendar.pages.main.MainPresenter;
import ru.test.domain.utils.MonthUtil;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * Created by oleg on 14.01.18.
 */

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    private MainPresenter presenter;
    @Mock private MainContract.View view;
    private SimpleDateFormat format = new SimpleDateFormat("LLLL ''yy");
    @Mock private MonthUtil util;

    @Before
    public void setUp() {

        presenter = new MainPresenter(format, util);
        presenter.attachView(view);

    }

    @Test
    public void testUpdateViewPagerItem() {

        given(util.getMonthNumber(any())).willReturn(1);

        presenter.updateViewPagerItem();

        verify(view).selectViewPagerItem(anyInt());

    }

    @Test
    public void testUpdateViewPagerItemError() {

        given(util.getMonthNumber(any())).willThrow(NullPointerException.class);

        presenter.updateViewPagerItem();

        verify(view).showToast(anyString());

    }

    @Test
    public void testSetTitle() {

        presenter.setTitle(1);

        verify(view).setTitle(anyString());
    }


}
