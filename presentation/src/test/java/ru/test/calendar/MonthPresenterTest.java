package ru.test.calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import ru.test.calendar.pages.month.MonthContract;
import ru.test.calendar.pages.month.MonthPresenter;
import ru.test.domain.data.entity.Day;
import ru.test.domain.data.entity.Month;
import ru.test.domain.repo.IMonthsRepository;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MonthPresenterTest {

    private MonthPresenter presenter;
    @Mock private MonthContract.View view;
    @Mock private IMonthsRepository monthsRepository;
    @Mock private Month month;

    @Before
    public void setUp() {
        presenter = new MonthPresenter(monthsRepository);
        presenter.attachView(view);
    }


    @Test
    public void testLoadMonthInfo() throws Exception {

        given(month.getDays()).willReturn(new ArrayList<>());
        given(monthsRepository.getMonthByNumber(1)).willReturn(month);

        presenter.loadMonthInfo(1);

        verify(view).updateView(any(Month.class));
        verify(view).updateAdapter(month.getDays());

    }
}