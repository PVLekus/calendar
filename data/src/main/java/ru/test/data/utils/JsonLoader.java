package ru.test.data.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by oleg on 13.01.18.
 */

public class JsonLoader {

    Context context;

    public JsonLoader(Context context) {
        this.context = context;
    }

    public String loadJson(String fileName) {

        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

}
