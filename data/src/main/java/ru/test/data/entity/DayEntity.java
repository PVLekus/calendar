package ru.test.data.entity;

/**
 * Created by oleg on 13.01.18.
 */

public class DayEntity {

    private String title;
    private int type;
    private String holiday;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }
}
