package ru.test.data.entity;

import java.util.ArrayList;

/**
 * Created by oleg on 13.01.18.
 */

public class ProductionСalendarEntity {

    private ArrayList<YearEntity> years = new ArrayList<>();

    public ArrayList<YearEntity> getYears() {
        return years;
    }

    public void setYears(ArrayList<YearEntity> years) {
        this.years = years;
    }
}
