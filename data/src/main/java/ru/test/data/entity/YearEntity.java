package ru.test.data.entity;

import java.util.ArrayList;

/**
 * Created by oleg on 13.01.18.
 */

public class YearEntity {

    private int number;
    private ArrayList<DayEntity> days;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<DayEntity> getDays() {
        return days;
    }

    public void setDays(ArrayList<DayEntity> days) {
        this.days = days;
    }
}
