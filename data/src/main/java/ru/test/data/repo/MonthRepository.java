package ru.test.data.repo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import ru.test.data.entity.DayEntity;
import ru.test.data.entity.ProductionСalendarEntity;
import ru.test.data.entity.YearEntity;
import ru.test.domain.data.entity.Day;
import ru.test.domain.data.entity.Month;
import ru.test.domain.repo.IMonthsRepository;
import ru.test.domain.utils.MonthUtil;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthRepository implements IMonthsRepository {

    ProductionСalendarEntity productionСalendar;
    MonthUtil util;
    SimpleDateFormat formatter;

    private Calendar calendarMaxDate = new GregorianCalendar(2019, Calendar.JANUARY, 1, 12, 00);

    public MonthRepository(ProductionСalendarEntity productionСalendar, MonthUtil util) {
        this.productionСalendar = productionСalendar;
        this.util = util;
        formatter = new SimpleDateFormat("MM.dd", Locale.US);
    }

    @Override
    public Month getMonthByNumber(int number) {

        Month month = getRawMonth(number);

        for (YearEntity year: productionСalendar.getYears()) {
            if (year.getNumber() == month.getYear()) {

                for (DayEntity dayEntity: year.getDays()) {

                    for (Day day: month.getDays()) {

                        if (dayEntity.getTitle().equals(day.getTitle())) {

                            day.setHoliday(dayEntity.getHoliday());

                            switch (dayEntity.getType()) {
                                case 1:
                                    day.setState(Day.DayType.HOLIDAY);
                                    break;
                                case 2:
                                    day.setState(Day.DayType.SHORT_DAY);
                                    break;
                                case 3:
                                    day.setState(Day.DayType.WORK_DAY);
                                    break;
                            }

                            break;
                        }

                    }

                }

                break;

            }
        }

        return month;

    }

    public Month getRawMonth(int number) {

        Calendar calendar = util.getMonthCalendar(number);
        int monthNumber = calendar.get(Calendar.MONTH);

        Month month = new Month();
        ArrayList<Day> days = new ArrayList<>();

        month.setYear(util.getYear(calendar));
        month.setMonth(monthNumber);
        month.setHalfYear(util.getHalfYear(calendar));
        month.setQuarter(util.getQuarter(calendar));

        int maxWeeks = calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);

        calendar.set(Calendar.WEEK_OF_MONTH, 1);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);


        for (int i = 0; i < maxWeeks*7; i++) {

            if (calendar.before(calendarMaxDate)) {
                Day day = new Day();
                day.setTitle(formatter.format(calendar.getTime()));
                day.setState(util.getDayType(calendar));
                day.setInMonth(util.isDayInMonth(calendar.getTime(), monthNumber));
                day.setNumber(calendar.get(Calendar.DAY_OF_MONTH));

                days.add(day);
            }

            calendar.add(Calendar.DAY_OF_MONTH, 1);

        }

        month.setDays(days);

        return month;

    }

}
