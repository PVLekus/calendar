package ru.test.domain.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import ru.test.domain.data.entity.Day;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthUtil {


    public int getMonthNumber(Date date) {

        int year = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.UK).format(date));
        int month = Integer.parseInt(new SimpleDateFormat("MM", Locale.UK).format(date));

        if (year < 2016) {
            return 1;
        }

        if (year > 2016) {
            month += 12;
        }

        if (year > 2017) {
            month += 12;
        }

        if (year > 2018) {
            return 36;
        }

        return month;
    }

    public Calendar getMonthCalendar(int monthNumber) {

        Calendar calendar = new GregorianCalendar(Locale.getDefault());
        calendar.set(2015, Calendar.DECEMBER, 1, 18, 0);
        calendar.add(Calendar.MONTH, monthNumber);
        return calendar;

    }

    public int getYear(Calendar calendar) {
        return calendar.get(Calendar.YEAR);
    }

    public int getHalfYear(Calendar calendar) {

        if (calendar.get(Calendar.MONTH) <= Calendar.JUNE) {
           return 1;
        } else {
           return 2;
        }
    }

    public int getQuarter(Calendar calendar) {

        final int monthNumber = calendar.get(Calendar.MONTH);

        if (monthNumber < 3) return 1;
        else if (monthNumber < 6) return 2;
        else if (monthNumber < 9) return 3;
        else return 4;

    }

    public Day.DayType getDayType(Calendar calendar) {

        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SATURDAY:
            case Calendar.SUNDAY:
                return Day.DayType.WEEKEND;
            default:
                return Day.DayType.WORK_DAY;

        }

    }

    public boolean isDayInMonth(Date date, int monthNumber) {

        Calendar calendar = new GregorianCalendar(Locale.getDefault());
        calendar.setTime(date);

        return calendar.get(Calendar.MONTH) == monthNumber;

    }

}
