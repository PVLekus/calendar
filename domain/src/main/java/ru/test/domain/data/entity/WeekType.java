package ru.test.domain.data.entity;

/**
 * Created by oleg on 14.01.18.
 */

public enum WeekType {

    HOURS40,
    HOURS36,
    HOURS24

}
