package ru.test.domain.data.entity;

/**
 * Created by oleg on 13.01.18.
 */

public class Day {

    private String title;
    private int number;
    private String holiday;
    private Boolean inMonth;
    private DayType state;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isInMonth() {
        return inMonth;
    }

    public void setInMonth(Boolean inMonth) {
        this.inMonth = inMonth;
    }

    public DayType getState() {
        return state;
    }

    public void setState(DayType state) {
        this.state = state;
    }

    public enum DayType {

        SHORT_DAY,
        WORK_DAY,
        HOLIDAY,
        WEEKEND,
        ANY

    }

}
