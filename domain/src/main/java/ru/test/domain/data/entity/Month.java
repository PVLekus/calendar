package ru.test.domain.data.entity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by oleg on 13.01.18.
 */

public class Month {

    private ArrayList<Day> days = new ArrayList<>();
    private int year;
    private int month;
    private int halfYear;
    private int quarter;

    public ArrayList<Day> getDays() {
        return days;
    }

    public void setDays(ArrayList<Day> days) {
        this.days = days;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHalfYear() {
        return halfYear;
    }

    public void setHalfYear(int halfYear) {
        this.halfYear = halfYear;
    }

    public int getQuarter() {
        return quarter;
    }

    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }

    private int getDays(Day.DayType dayType) {

        int count = 0;

        for (Day day: days) {
            if (day.isInMonth() && day.getState() == dayType) count++;
        }

        return count;

    }

    public int getWorkDays() {
        int workDays = getDays(Day.DayType.WORK_DAY);
        int shortDays = getDays(Day.DayType.SHORT_DAY);

        return workDays + shortDays;
    }

    public int getCalendarDays() {
        int count = 0;

        for (Day day: days) {
            if (day.isInMonth()) count++;
        }

        return count;
    }

    public int getHolidayAndWeekendDays() {
        int weekend = getDays(Day.DayType.WEEKEND);
        int holidays = getDays(Day.DayType.HOLIDAY);

        return weekend + holidays;
    }

    public ArrayList<String> getHolidaysNames() {

        ArrayList<String> holidays = new ArrayList<>();

        for (Day day: days) {
            if (day.isInMonth()
                    && day.getState() == Day.DayType.HOLIDAY
                    && day.getHoliday() != null
                    && !holidays.contains(day.getHoliday()))  holidays.add(day.getHoliday());
        }

        return holidays;

    }

    public String getWorkHours(WeekType type) {

        double hours = 0;

        int workDays = getDays(Day.DayType.WORK_DAY);
        int shortDays = getDays(Day.DayType.SHORT_DAY);

                switch (type) {
                    case HOURS40:
                        hours=(8 * workDays) + (7 * shortDays);
                        break;
                    case HOURS36:
                        hours=(7.2 * workDays) + (6.2 * shortDays);
                        break;
                    case HOURS24:
                        hours=(4.8 * workDays) + (3.8 * shortDays);
                        break;
                }
        return new DecimalFormat("##.##").format(hours);
    }

}
