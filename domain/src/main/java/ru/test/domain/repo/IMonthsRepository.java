package ru.test.domain.repo;

import ru.test.domain.data.entity.Month;

/**
 * Created by oleg on 13.01.18.
 */

public interface IMonthsRepository {

    Month getMonthByNumber(int number);

}
