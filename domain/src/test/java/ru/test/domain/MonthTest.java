package ru.test.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import ru.test.domain.data.entity.Day;
import ru.test.domain.data.entity.Month;
import ru.test.domain.data.entity.WeekType;

import static org.junit.Assert.*;

/**
 * Created by oleg on 14.01.18.
 */

public class MonthTest {

    Month month = new Month();

    @Before
    public void tune() {

        ArrayList<Day> list = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            Day day = new Day();
            day.setNumber(i);
            day.setInMonth(i > 2);
            if (i < 5) {
                day.setState(Day.DayType.WORK_DAY);
            } else if (i == 5) {
                day.setState(Day.DayType.WEEKEND);
            } else if (i == 6) {
                day.setState(Day.DayType.HOLIDAY);
            } else {
                day.setState(Day.DayType.SHORT_DAY);
            }

            list.add(day);

        }

        month.setDays(list);

    }

    @Test
    public void testWeekHours() {

        assertEquals("23", month.getWorkHours(WeekType.HOURS40));
        assertEquals("20,6", month.getWorkHours(WeekType.HOURS36));
        assertEquals("13,4", month.getWorkHours(WeekType.HOURS24));


    }

}
