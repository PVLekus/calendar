package ru.test.domain;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.test.domain.data.entity.Day;
import ru.test.domain.utils.MonthUtil;

/**
 * Created by oleg on 13.01.18.
 */

public class MonthUtilTest {

    MonthUtil converter;

    @Before
    public void prepare() {
        converter = new MonthUtil();
    }

    @Test
    public void testMonthNumberFromDate() {

        Date date = new Date(1515827677000L); //january 2018

        assertEquals(25, converter.getMonthNumber(date));

    }

    @Test
    public void testMonthNumberFromDateBefore2016() {

        Date date = new Date(1447200000000L); //11/11/2015

        assertEquals(1, converter.getMonthNumber(date));

    }

    @Test
    public void testMonthNumberFromDateAfter2018() {

        Date date = new Date(1546300800000L); //01/01/2019

        assertEquals(36, converter.getMonthNumber(date));

    }

    @Test
    public void testGetMonthNumber() {

        Calendar calendar = converter.getMonthCalendar(26);

        assertEquals(1, calendar.get(Calendar.MONTH));

    }

    @Test
    public void testGetYearNumber() {

        Calendar calendar = converter.getMonthCalendar(26);

        assertEquals(2018, calendar.get(Calendar.YEAR));

    }

    @Test
    public void testGetHalfYearOfJuly() {

        Calendar calendar = converter.getMonthCalendar(8);

        assertEquals(2, converter.getHalfYear(calendar));

    }

    @Test
    public void testGetHalfYearOfMay() {

        Calendar calendar = converter.getMonthCalendar(4);

        assertEquals(1, converter.getHalfYear(calendar));

    }

    @Test
    public void testDayTypeSaturdayAndSunday() {

        Date date = new Date(1515801640000L); //01/13/2018 Saturday
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        assertEquals(Day.DayType.WEEKEND, converter.getDayType(calendar));

        calendar.add(Calendar.DAY_OF_MONTH, 1);

        assertEquals(Day.DayType.WEEKEND, converter.getDayType(calendar));
    }

    @Test
    public void testGetQuarter() {

        Calendar calendar = new GregorianCalendar();
        calendar.set(2018, Calendar.JANUARY, 1);

        assertEquals(1, converter.getQuarter(calendar));

        calendar.set(2018, Calendar.JUNE, 1);

        assertEquals(2, converter.getQuarter(calendar));

        calendar.set(2018, Calendar.JULY, 1);

        assertEquals(3, converter.getQuarter(calendar));

        calendar.set(2018, Calendar.NOVEMBER, 1);

        assertEquals(4, converter.getQuarter(calendar));

    }





}
